/*
 * Copyright (c) 2021, ARM Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef MALLOC_WRAPPER_H
#define MALLOC_WRAPPER_H

#include <string.h>

/* definitions to control the compilation of malloc.c */
#define ABORT
#define HAVE_MMAP 0
#define LACKS_FCNTL_H 1
#define LACKS_SYS_MMAN_H 1
#define LACKS_SYS_PARAM_H 1
#define LACKS_SYS_TYPES_H 1
#define LACKS_UNISTD_H 1
#define LACKS_TIME_H 1
#define MALLOC_FAILURE_ACTION
#define NO_MALLOC_STATS 1
#define ONLY_MSPACES 1

typedef void* mspace;
mspace create_mspace_with_base(void* base, size_t capacity, int locked);
void* mspace_malloc(mspace msp, size_t bytes);
void* mspace_calloc(mspace msp, size_t n_elements, size_t elem_size);
void mspace_free(mspace msp, void* mem);
void init_mspace();

#endif /* MALLOC_WRAPPER_H */
