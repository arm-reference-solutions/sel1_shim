/*
 * Copyright (c) 2018-2021, ARM Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ASSERT_H
#define ASSERT_H

#include <cdefs.h>

#if ENABLE_ASSERTIONS
#define assert(e)	((e) ? (void)0 : __assert(__FILE__, __LINE__, #e))
#else
#define assert(e)	((void)0)
#endif /* ENABLE_ASSERTIONS */

__dead2 void __assert(const char *file, unsigned int line,
		      const char *assertion);

#define COMPILE_TIME_ASSERT(x) \
        do { \
                switch (0) { case 0: case ((x) ? 1: 0): default : break; } \
        } while (0)

#endif /* ASSERT_H */
