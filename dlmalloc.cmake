#-------------------------------------------------------------------------------
# Copyright (c) 2021, Arm Limited and Contributors. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause
#
#-------------------------------------------------------------------------------

set(DLMALLOC_SOURCE_DIR "${CMAKE_CURRENT_BINARY_DIR}/dl-malloc-src" CACHE PATH "Path to DL malloc source.")
set(DLMALLOC_URL "https://raw.githubusercontent.com/ARMmbed/dlmalloc/75149a9d7c50442db0f89085baab4f8eef519104/source/" CACHE STRING "URL to download dl-malloc files from.")

set(_FILE "${DLMALLOC_SOURCE_DIR}/malloc.c")
function(get_dlmalloc_if_needed)
    if (NOT EXISTS ${_FILE})
	    file(DOWNLOAD "${DLMALLOC_URL}/dlmalloc.c" ${_FILE} EXPECTED_MD5 a2973f1be9a71842ec1ead2f1c515e7c)
    endif()
    if(DLMALLOC_SOURCE_DIR STREQUAL "${CMAKE_CURRENT_BINARY_DIR}/dl-malloc-src")
        set_property(SOURCE "${_FILE}" PROPERTY GENERATED 1)
    endif()
endfunction()

get_dlmalloc_if_needed()

target_sources(${TGT} PRIVATE "${_FILE}")

set_source_files_properties("${_FILE}" PROPERTIES COMPILE_FLAGS "-include malloc_wrapper.h")
