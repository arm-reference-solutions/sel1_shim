/* SPDX-License-Identifier: BSD-2-Clause */
/*
 * Copyright (c) 2014, STMicroelectronics International N.V.
 * Copyright (c) 2021, Arm Limited.
 */

#ifndef TRACE_H
#define TRACE_H

#include <debug.h>

#define EMSG(...)   mp_printf("ERROR:   " __VA_ARGS__)
#define DMSG(...)   mp_printf("NOTICE:   " __VA_ARGS__)

#endif /* TRACE_H */
