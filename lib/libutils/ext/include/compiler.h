/* SPDX-License-Identifier: BSD-2-Clause */
/*
 * Copyright (c) 2014, STMicroelectronics International N.V.
 * Copyright (c) 2021, Arm Limited.
 */

#ifndef COMPILER_H
#define COMPILER_H

#define __packed  	__attribute__((__packed__))
#define __noreturn	__attribute__((__noreturn__))
#define __maybe_unused	__attribute__((__unused__))

#define __GCC_VERSION (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + \
		       __GNUC_PATCHLEVEL__)

#if __GCC_VERSION >= 50100 && !defined(__CHECKER__)
#define __HAVE_BUILTIN_OVERFLOW 1
#endif

#ifdef __HAVE_BUILTIN_OVERFLOW
#define __compiler_add_overflow(a, b, res) \
	__builtin_add_overflow((a), (b), (res))

#define __compiler_sub_overflow(a, b, res) \
	__builtin_sub_overflow((a), (b), (res))

#define __compiler_mul_overflow(a, b, res) \
	__builtin_mul_overflow((a), (b), (res))
#endif /*!__HAVE_BUILTIN_OVERFLOW*/

#endif /*COMPILER_H*/
