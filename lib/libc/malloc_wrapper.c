/*
 * Copyright (c) 2021, ARM Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <malloc_wrapper.h>

static mspace shim_mspace;

void init_mspace(void* base, size_t capacity)
{
        shim_mspace = create_mspace_with_base(base, capacity, 0);
}

void *malloc(size_t size)
{
        return mspace_malloc(shim_mspace, size);
}

void *calloc(size_t nitems, size_t size)
{
        return mspace_calloc(shim_mspace, nitems, size);
}

void free(void *mem)
{
        mspace_free(shim_mspace, mem);
}
