Build Instructions
==================

Currently, this project doesn't support the standalone build. It will be
cloned during the build of any Trusted Service Secure Partition. The resulting
binary will contain the code for both the shim layer source code (S-EL1) and
the secure services source code (S-EL0). The separation between the S-EL1 and
S-EL0 code will be handled by the linker script.

It supports the CMake based build system. It can be added as a component in
any CMake based project.

--------------

*Copyright (c) 2021, Arm Limited and Contributors. All rights reserved.*

SPDX-License-Identifier: BSD-3-Clause
