License
=======

This project is provided under the terms of the 3 Clause Berkeley Software
Distribution license (BSD-3-Clause) as provided in
:ref:`this license <BSD-3-Clause>`.

However, there are some individual files provided under the terms of the 2
Clause Berkeley Software Distribution license (BSD-2-Clause) as provided in
:ref:`this license <BSD-2-Clause>`.

Every file contains a SPDX License Identifier which helps to identify its
associated license.

--------------

*Copyright (c) 2021, Arm Limited and Contributors. All rights reserved.*

SPDX-License-Identifier: BSD-3-Clause
