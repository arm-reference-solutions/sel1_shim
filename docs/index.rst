Welcome to S-EL1 Shim layer's documentation!
============================================
The primary purpose of the S-EL1 shim layer is to execute the Trusted Services
with Hafnium as the Secure Partition Manager. Since Hafnium currently
doesn't support the execution of the S-EL0 Secure Partitions, the addition of
the S-EL1 shim layer to the Trusted Service SP will make it as a S-EL1 secure
partition. Even though the S-EL1 shim layer is designed specifically to execute
the trusted services, Hafnium should be able to run any userspace application
on S-EL0 with this shim layer at S-EL1.

The primary funcion of the shim layer is to forward the FFA messages between
the SPMC and SP. It initializes the memory for the shim layer and for the
secure partitions and also initializes the serial console.

Most of the code is ported from `tf-a-tests`_ and a small portion from `optee`_.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   build-instructions
   license


--------------

.. _tf-a-tests: https://git.trustedfirmware.org/TF-A/tf-a-tests.git/
.. _optee: https://git.trustedfirmware.org/OP-TEE/optee_os.git/

*Copyright (c) 2021, Arm Limited and Contributors. All rights reserved.*

SPDX-License-Identifier: BSD-3-Clause
