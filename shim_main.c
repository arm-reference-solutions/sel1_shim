/*
 * Copyright (c) 2021, Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <assert.h>
#include <debug.h>
#include <ffa_helpers.h>
#include <lib/aarch64/arch_helpers.h>
#include <lib/xlat_tables/xlat_mmu_helpers.h>
#include <lib/xlat_tables/xlat_tables_v2.h>
#include <platform_def.h>
#include <sp_debug.h>
#include <sp_helpers.h>

#include <shim_sp.h>
#include <sp_def.h>

static void shim_print_memory_layout(void)
{
	NOTICE("Secure Partition memory layout:\n");

	NOTICE("  Image regions\n");
	NOTICE("    Text region            : %p - %p\n",
		(void *)SP_TEXT_START, (void *)SP_TEXT_END);
	NOTICE("    Read-only data region  : %p - %p\n",
		(void *)SP_RODATA_START, (void *)SP_RODATA_END);
	NOTICE("    Data region            : %p - %p\n",
		(void *)SP_DATA_START, (void *)SP_DATA_END);
	NOTICE("    BSS region             : %p - %p\n",
		(void *)SP_BSS_START, (void *)SP_BSS_END);
	NOTICE("    Total image memory     : %p - %p\n",
		(void *)SP_IMAGE_BASE,
		(void *)(SP_IMAGE_BASE + SP_IMAGE_SIZE));
}

static void shim_plat_configure_mmu()
{
	NOTICE("SHIM_TEXT_START = %p\n", (void *)SHIM_TEXT_START);
	mmap_add_region(SHIM_TEXT_START,
			SHIM_TEXT_START,
			SHIM_TEXT_END - SHIM_TEXT_START,
			MT_CODE | MT_PRIVILEGED);
	mmap_add_region(SHIM_RODATA_START,
			SHIM_RODATA_START,
			SHIM_RODATA_END - SHIM_RODATA_START,
			MT_RO_DATA | MT_PRIVILEGED);
	mmap_add_region(SHIM_DATA_START,
			SHIM_DATA_START,
			SHIM_DATA_END - SHIM_DATA_START,
			MT_RW_DATA | MT_PRIVILEGED);
	mmap_add_region(SHIM_BSS_START,
			SHIM_BSS_START,
			SHIM_BSS_END - SHIM_BSS_START,
			MT_RW_DATA | MT_PRIVILEGED);
	mmap_add_region(SP_TEXT_START,
			SP_TEXT_START,
			SP_TEXT_END - SP_TEXT_START,
			MT_CODE | MT_USER);
	mmap_add_region(SP_RODATA_START,
			SP_RODATA_START,
			SP_RODATA_END - SP_RODATA_START,
			MT_RO_DATA | MT_USER);
	mmap_add_region(SP_DATA_START,
			SP_DATA_START,
			SP_DATA_END - SP_DATA_START,
			MT_RW_DATA | MT_USER);
	mmap_add_region(SP_BSS_START,
			SP_BSS_START,
			SP_BSS_END - SP_BSS_START,
			MT_RW_DATA | MT_USER);

/* map the kernel address space in the secure partition address space
 * to access the buffer created in the non-secure kernel.
 */
	mmap_add_region(KERNEL_IMAGE_BASE,
			KERNEL_IMAGE_BASE,
			KERNEL_IMAGE_SIZE,
			MT_RW_DATA | MT_USER | MT_NS | MT_EXECUTE_NEVER);

	mmap_add_region(TC_DRAM2_BASE,
			TC_DRAM2_BASE,
			TC_DRAM2_SIZE,
			MT_RW_DATA | MT_USER | MT_NS | MT_EXECUTE_NEVER);

/* map the crypto address space in the secure storage partition to access the
 * buffer created by crypto partition
 */
#if defined(INTERNAL_TRUSTED_STORAGE)
	mmap_add_region(CRYPTO_IMAGE_BASE,
			CRYPTO_IMAGE_BASE,
			CRYPTO_IMAGE_SIZE,
			MT_RW_DATA | MT_USER | MT_EXECUTE_NEVER);
#endif

/* map the nor flash address space in the firmare update partition to be able
 * to update the firmware banks
 */
#ifdef FIRMWARE_UPDATE
	mmap_add_region(FLASH_BASE, FLASH_BASE, FLASH_SIZE,
			MT_RW_DATA | MT_USER | MT_NS | MT_EXECUTE_NEVER);
#endif
	init_xlat_tables();
}

int shim_main(void)
{
	assert(IS_IN_EL1() != 0);

	/* Initialise console */
	set_putc_impl(HVC_CALL_AS_STDOUT);

	NOTICE("Booting S-EL1 Shim\n");

	/* Configure and enable Stage-1 MMU, enable D-Cache */
	shim_plat_configure_mmu();
	enable_mmu_el1(0);

	shim_print_memory_layout();

	set_putc_impl(SVC_CALL_AS_STDOUT);

	return 0;
}
