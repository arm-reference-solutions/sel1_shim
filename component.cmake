#-------------------------------------------------------------------------------
# Copyright (c) 2021, Arm Limited and Contributors. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause
#
#-------------------------------------------------------------------------------

if (NOT DEFINED TGT)
	message(FATAL_ERROR "mandatory parameter TGT is not defined.")
endif()

if (NOT DEFINED TS_PLATFORM)
	message(FATAL_ERROR "mandatory parameter TS_PLATFORM is not defined.")
endif()

if (TS_PLATFORM STREQUAL "arm/total_compute")
	target_sources(${TGT} PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/plat/arm/tc/aarch64/plat_helpers.S"
	)
	set(PLAT_HEADER_FILES
		${CMAKE_CURRENT_LIST_DIR}/plat/arm/tc/include
	)
else()
	message(FATAL_ERROR "Platform ${TS_PLATFORM} is not supported by shim")
endif()

include("${CMAKE_CURRENT_LIST_DIR}/dlmalloc.cmake")

target_sources(${TGT} PRIVATE
	"${CMAKE_CURRENT_LIST_DIR}/lib/aarch64/cache_helpers.S"
	"${CMAKE_CURRENT_LIST_DIR}/lib/aarch64/misc_helpers.S"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/assert.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/memchr.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/memcmp.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/memcpy.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/memmove.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/memset.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/printf.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/putchar.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/rand.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/snprintf.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/strchr.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/strcmp.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/strlen.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libc/malloc_wrapper.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libutils/isoc/newlib/strcpy.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libutils/isoc/newlib/strncpy.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/libutils/isoc/newlib/strstr.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/locks/aarch64/spinlock.S"
	"${CMAKE_CURRENT_LIST_DIR}/lib/smc/aarch64/asm_smc.S"
	"${CMAKE_CURRENT_LIST_DIR}/lib/smc/aarch64/hvc.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/utils/mp_printf.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/xlat_tables_v2/aarch64/enable_mmu.S"
	"${CMAKE_CURRENT_LIST_DIR}/lib/xlat_tables_v2/aarch64/xlat_tables_arch.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/xlat_tables_v2/xlat_tables_context.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/xlat_tables_v2/xlat_tables_core.c"
	"${CMAKE_CURRENT_LIST_DIR}/lib/xlat_tables_v2/xlat_tables_utils.c"
	"${CMAKE_CURRENT_LIST_DIR}/drivers/arm/pl011/aarch64/pl011_console.S"
	"${CMAKE_CURRENT_LIST_DIR}/shim_main.c"
	"${CMAKE_CURRENT_LIST_DIR}/spm/aarch64/spm_shim_entrypoint.S"
	"${CMAKE_CURRENT_LIST_DIR}/spm/aarch64/spm_shim_exceptions.S"
	"${CMAKE_CURRENT_LIST_DIR}/spm/aarch64/sp_entrypoint.S"
	"${CMAKE_CURRENT_LIST_DIR}/spm/common/aarch64/sp_arch_helpers.S"
	"${CMAKE_CURRENT_LIST_DIR}/spm/common/sp_debug.c"
	"${CMAKE_CURRENT_LIST_DIR}/spm/common/sp_helpers.c"
	"${CMAKE_CURRENT_LIST_DIR}/tftf/framework/aarch64/asm_debug.S"
	"${CMAKE_CURRENT_LIST_DIR}/tftf/framework/debug.c"
	)

set(SHIM_HEADER_FILES
	${CMAKE_CURRENT_LIST_DIR}
	${CMAKE_CURRENT_LIST_DIR}/include
	${CMAKE_CURRENT_LIST_DIR}/include/common
	${CMAKE_CURRENT_LIST_DIR}/include/common/aarch64
	${CMAKE_CURRENT_LIST_DIR}/include/drivers
	${CMAKE_CURRENT_LIST_DIR}/include/lib
	${CMAKE_CURRENT_LIST_DIR}/include/lib/aarch64
	${CMAKE_CURRENT_LIST_DIR}/include/lib/libc
	${CMAKE_CURRENT_LIST_DIR}/include/lib/libc/aarch64
	${CMAKE_CURRENT_LIST_DIR}/include/lib/xlat_tables
	${CMAKE_CURRENT_LIST_DIR}/include/lib/xlat_tables/aarch64
	${CMAKE_CURRENT_LIST_DIR}/include/runtime_services
	${CMAKE_CURRENT_LIST_DIR}/lib/libutils/ext/include
	${CMAKE_CURRENT_LIST_DIR}/lib/libutils/isoc/include
	${CMAKE_CURRENT_LIST_DIR}/lib/libutils/isoc/newlib
	${CMAKE_CURRENT_LIST_DIR}/lib/xlat_tables_v2
	${CMAKE_CURRENT_LIST_DIR}/spm
	${CMAKE_CURRENT_LIST_DIR}/spm/common
	${CMAKE_CURRENT_LIST_DIR}/tftf/framework/include
)

target_include_directories(${TGT} PRIVATE ${SHIM_HEADER_FILES})
target_include_directories(${TGT} PRIVATE ${PLAT_HEADER_FILES})

target_compile_definitions(${TGT} PRIVATE
	LOG_LEVEL=30
)

if(TARGET internal-trusted-storage)
	set(SHIM_DEFINES
		INTERNAL_TRUSTED_STORAGE=1
		__ASSEMBLY__=1
	)
elseif(TARGET crypto-sp)
	set(SHIM_DEFINES
		CRYPTO=1
		__ASSEMBLY__=1
	)
elseif(TARGET firmware-update)
	set(SHIM_DEFINES
		FIRMWARE_UPDATE=1
		__ASSEMBLY__=1
	)
endif()

set(LD_OUTPUT
	${CMAKE_BINARY_DIR}/sp.ld
)

compiler_preprocess_file(
	SRC ${CMAKE_CURRENT_LIST_DIR}/sp.ld.S
	DST ${LD_OUTPUT}
	DEFINES ${SHIM_DEFINES}
	INCLUDES ${SHIM_HEADER_FILES} ${PLAT_HEADER_FILES}
)

add_custom_target(${TGT}-pplscript DEPENDS ${LD_OUTPUT})
add_dependencies(${TGT} ${TGT}-pplscript)
