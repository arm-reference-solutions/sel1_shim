S-EL1 Shim Layer
================

This repository holds the S-EL1 Shim layer source code.

The full documentation of this project is `Sphinx`_ based, lives in the *doc*
sub-directory and is captured in reStructuredText_ format.

For licensing information please refer to `license.rst`_

.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _Sphinx: http://www.sphinx-doc.org/en/master/
.. _`license.rst`: docs/license.rst

--------------

*Copyright (c) 2021, Arm Limited and Contributors. All rights reserved.*

SPDX-License-Identifier: BSD-3-Clause
